use crate::sidebar::tag_list::models::TagListTagModel;
use crate::util::GtkUtil;
use gtk::{prelude::*, subclass::prelude::*, CompositeTemplate, Image, Label};
use news_flash::models::TagID;
use parking_lot::RwLock;
use std::str;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/resources/ui/tag.ui")]
    pub struct TagRow {
        pub id: RwLock<TagID>,

        #[template_child]
        pub tag_title: TemplateChild<Label>,
        #[template_child]
        pub tag_color: TemplateChild<Image>,
    }

    impl Default for TagRow {
        fn default() -> Self {
            TagRow {
                id: RwLock::new(TagID::new("")),
                tag_title: TemplateChild::default(),
                tag_color: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TagRow {
        const NAME: &'static str = "TagRow";
        type ParentType = gtk::ListBoxRow;
        type Type = super::TagRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for TagRow {}

    impl WidgetImpl for TagRow {}

    impl ContainerImpl for TagRow {}

    impl BinImpl for TagRow {}

    impl ListBoxRowImpl for TagRow {}
}

glib::wrapper! {
    pub struct TagRow(ObjectSubclass<imp::TagRow>)
        @extends gtk::Widget, gtk::ListBoxRow;
}

impl TagRow {
    pub fn new(model: &TagListTagModel) -> Self {
        let row = glib::Object::new::<Self>(&[]).unwrap();
        let imp = imp::TagRow::from_instance(&row);

        *imp.id.write() = model.id.clone();

        let tag_color_circle = imp.tag_color.get();
        let color = model.color.clone();
        imp.tag_color.connect_realize(move |_widget| {
            if let Some(window) = tag_color_circle.window() {
                let scale = GtkUtil::get_scale(&tag_color_circle);
                if let Some(surface) = GtkUtil::generate_color_cirlce(&window, color.as_deref(), scale) {
                    tag_color_circle.set_from_surface(Some(&surface));
                }
            }
        });

        row.update_title(&model.label);
        row
    }

    pub fn update_title(&self, title: &str) {
        let imp = imp::TagRow::from_instance(self);
        imp.tag_title.set_label(title);
    }

    pub fn tag_id(&self) -> TagID {
        let imp = imp::TagRow::from_instance(self);
        imp.id.read().clone()
    }

    pub fn title(&self) -> String {
        let imp = imp::TagRow::from_instance(self);
        imp.tag_title.text().as_str().to_owned()
    }
}
