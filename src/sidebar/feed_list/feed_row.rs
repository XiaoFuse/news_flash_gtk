use crate::app::Action;
use crate::main_window_state::MainWindowState;
use crate::sidebar::feed_list::models::FeedListFeedModel;
use crate::undo_bar::UndoActionModel;
use crate::util::{GtkUtil, Util};
use cairo::{self, Format, ImageSurface};
use futures::channel::oneshot;
use futures::future::FutureExt;
use gdk::{DragAction, EventType, ModifierType};
use gio::{traits::ActionMapExt, Menu, MenuItem, SimpleAction};
use glib::{clone, Continue, Sender, Source, SourceId};
use gtk::{
    self, prelude::WidgetExtManual, Box, EventBox, Image, Inhibit, Label, Popover, PositionType, Revealer, StateFlags,
    TargetEntry, TargetFlags,
};
use gtk::{prelude::*, subclass::prelude::*, CompositeTemplate};
use log::warn;
use news_flash::models::{CategoryID, FavIcon, FeedID, PluginCapabilities};
use parking_lot::RwLock;
use std::str;
use std::sync::Arc;
use std::time::Duration;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/resources/ui/feed.ui")]
    pub struct FeedRow {
        pub id: RwLock<FeedID>,
        pub parent_id: RwLock<CategoryID>,
        pub hide_timeout: Arc<RwLock<Option<SourceId>>>,

        #[template_child]
        pub item_count_label: TemplateChild<Label>,
        #[template_child]
        pub item_count_event: TemplateChild<EventBox>,
        #[template_child]
        pub title: TemplateChild<Label>,
        #[template_child]
        pub revealer: TemplateChild<Revealer>,
        #[template_child]
        pub favicon: TemplateChild<Image>,
        #[template_child]
        pub level_margin: TemplateChild<Box>,
    }

    impl Default for FeedRow {
        fn default() -> Self {
            FeedRow {
                id: RwLock::new(FeedID::new("")),
                parent_id: RwLock::new(CategoryID::new("")),
                hide_timeout: Arc::new(RwLock::new(None)),
                item_count_label: TemplateChild::default(),
                item_count_event: TemplateChild::default(),
                title: TemplateChild::default(),
                revealer: TemplateChild::default(),
                favicon: TemplateChild::default(),
                level_margin: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FeedRow {
        const NAME: &'static str = "FeedRow";
        type ParentType = gtk::ListBoxRow;
        type Type = super::FeedRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for FeedRow {}

    impl WidgetImpl for FeedRow {}

    impl ContainerImpl for FeedRow {}

    impl BinImpl for FeedRow {}

    impl ListBoxRowImpl for FeedRow {}
}

glib::wrapper! {
    pub struct FeedRow(ObjectSubclass<imp::FeedRow>)
        @extends gtk::Widget, gtk::Bin, gtk::ListBoxRow;
}

impl FeedRow {
    pub fn new(
        model: &FeedListFeedModel,
        state: &Arc<RwLock<MainWindowState>>,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
        visible: bool,
        sender: Sender<Action>,
    ) -> Self {
        let row = glib::Object::new::<Self>(&[]).unwrap();
        let imp = imp::FeedRow::from_instance(&row);

        imp.level_margin.set_margin_start(model.level * 24);
        *imp.id.write() = model.id.clone();
        *imp.parent_id.write() = model.parent_id.clone();

        Self::setup_row(
            &row,
            &sender,
            state,
            features,
            &imp.revealer,
            &model.id,
            &model.parent_id,
            model.label.clone(),
        );

        row.update_item_count(model.item_count);
        row.update_title(&model.label);
        row.update_favicon(&model.id, &sender);
        if !visible {
            row.collapse();
        }
        row
    }

    fn setup_row(
        row: &FeedRow,
        sender: &Sender<Action>,
        window_state: &Arc<RwLock<MainWindowState>>,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
        revealer: &gtk::Revealer,
        id: &FeedID,
        parent_id: &CategoryID,
        label: String,
    ) {
        row.set_activatable(true);
        row.style_context().remove_class("activatable");

        let mut support_mutation = false;
        if let Some(features) = features.read().as_ref() {
            support_mutation = features.contains(PluginCapabilities::ADD_REMOVE_FEEDS)
                && features.contains(PluginCapabilities::MODIFY_CATEGORIES);
        }

        if support_mutation {
            let entry = TargetEntry::new("FeedRow", TargetFlags::SAME_APP, 0);
            revealer.drag_source_set(ModifierType::BUTTON1_MASK, &[entry], DragAction::MOVE);
            revealer.drag_source_add_text_targets();

            revealer.connect_drag_data_get(clone!(
                @strong parent_id,
                @strong id as feed_id => @default-panic, move |_widget, _ctx, selection_data, _info, _time|
            {
                if let Ok(feed_id_json) = serde_json::to_string(&feed_id.clone()) {
                    if let Ok(category_id_json) = serde_json::to_string(&parent_id.clone()) {
                        let mut data = String::from("FeedID ");
                        data.push_str(&feed_id_json);
                        data.push_str(";");
                        data.push_str(&category_id_json);
                        selection_data.set_text(&data);
                    }
                }
            }));

            revealer.connect_drag_begin(clone!(@weak row => @default-panic, move |_widget, drag_context| {
                let alloc = row.allocation();
                let surface = ImageSurface::create(Format::ARgb32, alloc.width, alloc.height)
                    .expect("Failed to create Cairo ImageSurface.");
                let cairo_context = cairo::Context::new(&surface).expect("Failed to create cairo context");
                let style_context = row.style_context();
                style_context.add_class("drag-icon");
                row.draw(&cairo_context);
                style_context.remove_class("drag-icon");
                drag_context.drag_set_icon_surface(&surface);
            }));

            row.connect_button_press_event(clone!(
                @strong id as feed_id,
                @strong parent_id,
                @strong label,
                @weak window_state,
                @strong sender => @default-panic, move |row, event|
            {
                if event.button() != 3 {
                    return Inhibit(false);
                }

                match event.event_type() {
                    EventType::ButtonRelease | EventType::DoubleButtonPress | EventType::TripleButtonPress => {
                        return Inhibit(false)
                    }
                    _ => {}
                }

                if window_state.read().get_offline() {
                    return Inhibit(false);
                }

                let model = Menu::new();

                let rename_feed_dialog_action = SimpleAction::new(&format!("rename-feed-{}-dialog", feed_id), None);
                rename_feed_dialog_action.connect_activate(clone!(
                    @weak row,
                    @strong feed_id,
                    @strong parent_id,
                    @strong sender => @default-panic, move |_action, _parameter|
                {
                    Util::send(&sender, Action::RenameFeedDialog(feed_id.clone(), parent_id.clone()));

                    if let Ok(main_window) = GtkUtil::get_main_window(&row) {
                        main_window.remove_action(&format!("rename-feed-{}-dialog", feed_id));
                    }
                }));

                let rename_feed_item = MenuItem::new(Some("Rename"), None);
                rename_feed_item.set_action_and_target_value(Some(&format!("rename-feed-{}-dialog", feed_id)), None);
                model.append_item(&rename_feed_item);

                let delete_feed_item = MenuItem::new(Some("Delete"), None);
                let delete_feed_action = SimpleAction::new(&format!("enqueue-delete-feed-{}", feed_id), None);
                delete_feed_action.connect_activate(clone!(
                    @weak row,
                    @strong label,
                    @strong feed_id,
                    @strong sender => @default-panic, move |_action, _parameter|
                {
                    let remove_action = UndoActionModel::DeleteFeed(feed_id.clone(), label.clone());
                    Util::send(&sender, Action::UndoableAction(remove_action));

                    if let Ok(main_window) = GtkUtil::get_main_window(&row) {
                        main_window.remove_action(&format!("enqueue-delete-feed-{}", feed_id));
                    }
                }));

                if let Ok(main_window) = GtkUtil::get_main_window(row) {
                    main_window.add_action(&delete_feed_action);
                    main_window.add_action(&rename_feed_dialog_action);
                }
                delete_feed_item.set_action_and_target_value(Some(&format!("enqueue-delete-feed-{}", feed_id)), None);
                model.append_item(&delete_feed_item);

                let popover = Popover::new(Some(row));
                popover.set_position(PositionType::Bottom);
                popover.bind_model(Some(&model), Some("win"));
                popover.show();
                popover.connect_closed(clone!(@weak row => @default-panic, move |_popover| {
                    row.unset_state_flags(StateFlags::PRELIGHT);
                }));
                row.set_state_flags(StateFlags::PRELIGHT, false);

                Inhibit(true)
            }));
        }
    }

    pub fn update_item_count(&self, count: i64) {
        let imp = imp::FeedRow::from_instance(self);

        if count > 0 {
            imp.item_count_label.set_label(&count.to_string());
            imp.item_count_event.set_visible(true);
        } else {
            imp.item_count_event.set_visible(false);
        }
    }

    fn update_favicon(&self, feed_id: &FeedID, global_sender: &Sender<Action>) {
        let imp = imp::FeedRow::from_instance(self);
        let (sender, receiver) = oneshot::channel::<Option<FavIcon>>();
        Util::send(global_sender, Action::LoadFavIcon((feed_id.clone(), sender)));

        let favicon = imp.favicon.get();
        let scale = GtkUtil::get_scale(&favicon);
        let glib_future = receiver.map(move |res| match res {
            Ok(Some(icon)) => {
                if let Some(data) = &icon.data {
                    if let Ok(surface) = GtkUtil::create_surface_from_bytes(data, 16, 16, scale) {
                        favicon.set_from_surface(Some(&surface));
                    }
                }
            }
            Ok(None) => {
                warn!("Favicon does not contain image data.");
            }
            Err(_) => warn!("Receiving favicon failed."),
        });

        Util::glib_spawn_future(glib_future);
    }

    pub fn update_title(&self, title: &str) {
        let imp = imp::FeedRow::from_instance(self);
        imp.title.set_label(title);
    }

    pub fn collapse(&self) {
        let imp = imp::FeedRow::from_instance(self);
        imp.revealer.set_reveal_child(false);
        imp.revealer.style_context().add_class("hidden");
        self.set_selectable(false);

        // hide row after animation finished
        {
            // add new timeout
            //let hide_timeout = imp.
            let source_id = glib::timeout_add_local(
                Duration::from_millis(250),
                clone!(
                    @weak imp.hide_timeout as hide_timeout,
                    @weak self as widget => @default-panic, move ||
                {
                    widget.set_visible(false);
                    hide_timeout.write().take();
                    Continue(false)
                }),
            );
            imp.hide_timeout.write().replace(source_id);
        }
    }

    pub fn expand(&self) {
        let imp = imp::FeedRow::from_instance(self);
        // clear out timeout to fully hide row
        {
            if let Some(source_id) = imp.hide_timeout.write().take() {
                if Source::remove(source_id).is_ok() {
                    // log something
                };
                // log something
            }
        }

        self.set_visible(true);
        imp.revealer.set_reveal_child(true);
        imp.revealer.style_context().remove_class("hidden");
        self.set_selectable(true);
    }

    pub fn disable_dnd(&self) {
        let imp = imp::FeedRow::from_instance(self);
        imp.revealer.drag_source_unset();
    }

    pub fn enable_dnd(&self) {
        let imp = imp::FeedRow::from_instance(self);
        let entry = TargetEntry::new("FeedRow", TargetFlags::SAME_APP, 0);
        imp.revealer
            .drag_source_set(ModifierType::BUTTON1_MASK, &[entry], DragAction::MOVE);
    }

    pub fn feed_id(&self) -> FeedID {
        let imp = imp::FeedRow::from_instance(self);
        imp.id.read().clone()
    }

    pub fn parent_id(&self) -> CategoryID {
        let imp = imp::FeedRow::from_instance(self);
        imp.parent_id.read().clone()
    }
}
