use super::theme_chooser::ThemeChooser;
use crate::app::Action;
use crate::settings::Settings;
use crate::util::{BuilderHelper, GtkUtil, Util};
use gdk::{EventMask, EventType};
use glib::{clone, Sender, SignalHandlerId};
use gtk::{prelude::*, EventBox, FontButton, Inhibit, Label, ListBox, Popover, Switch};
use libhandy::{ActionRow, PreferencesPage, PreferencesWindow};
use news_flash::models::ArticleOrder;
use parking_lot::RwLock;
use std::sync::Arc;

pub struct ViewsPage {
    pub widget: PreferencesPage,
    delete_signal: Arc<RwLock<Option<SignalHandlerId>>>,
    sender: Sender<Action>,
    settings: Arc<RwLock<Settings>>,

    feed_only_show_relevant_switch: Switch,
    feed_only_show_relevant_signal: Arc<RwLock<Option<SignalHandlerId>>>,

    article_order_pop: Popover,
    article_order_list: ListBox,
    article_order_list_signal: Arc<RwLock<Option<SignalHandlerId>>>,
    article_order_label: Label,
    article_order_event: EventBox,
    article_order_event_signal: Arc<RwLock<Option<SignalHandlerId>>>,
    article_order_row: ActionRow,
    article_order_listbox_signal: Arc<RwLock<Option<SignalHandlerId>>>,

    show_thumbs_switch: Switch,
    show_thumbs_signal: Arc<RwLock<Option<SignalHandlerId>>>,

    article_theme_label: Label,
    article_theme_row: ActionRow,
    article_theme_listbox_signal: Arc<RwLock<Option<SignalHandlerId>>>,
    article_theme_event: EventBox,
    article_theme_event_signal: Arc<RwLock<Option<SignalHandlerId>>>,

    allow_selection_switch: Switch,
    allow_selection_switch_signal: Arc<RwLock<Option<SignalHandlerId>>>,

    font_row: ActionRow,
    font_button: FontButton,
    font_button_signal: Arc<RwLock<Option<SignalHandlerId>>>,

    use_system_font_switch: Switch,
    use_system_font_switch_signal: Arc<RwLock<Option<SignalHandlerId>>>,
}

impl ViewsPage {
    pub fn new(parent: &PreferencesWindow, sender: &Sender<Action>, settings: &Arc<RwLock<Settings>>) -> Self {
        let builder = BuilderHelper::new("settings_views");
        let views_page = builder.get::<PreferencesPage>("views_page");

        // -----------------------------------------------------------------------
        // feed list show only relevant
        // -----------------------------------------------------------------------
        let feed_only_show_relevant_switch = builder.get::<Switch>("filter_feeds_switch");
        feed_only_show_relevant_switch.set_state(settings.read().get_feed_list_only_show_relevant());

        // -----------------------------------------------------------------------
        // article list order
        // -----------------------------------------------------------------------
        let article_order_label = builder.get::<Label>("article_order_label");
        article_order_label.set_label(settings.read().get_article_list_order().to_str());

        let article_order_list = builder.get::<ListBox>("article_order_list");
        let article_order_row = builder.get::<ActionRow>("article_order_row");

        let article_order_event = builder.get::<EventBox>("article_order_event");
        article_order_event.set_events(EventMask::BUTTON_PRESS_MASK);
        let article_order_pop = builder.get::<Popover>("article_order_pop");

        // -----------------------------------------------------------------------
        // article list show thumbnails
        // -----------------------------------------------------------------------
        let show_thumbs_switch = builder.get::<Switch>("article_list_thumbs_switch");
        show_thumbs_switch.set_state(settings.read().get_article_list_show_thumbs());

        // -----------------------------------------------------------------------
        // article view theme
        // -----------------------------------------------------------------------
        let article_theme_label = builder.get::<Label>("article_theme_label");
        article_theme_label.set_label(settings.read().get_article_view_theme().name());

        let article_theme_row = builder.get::<ActionRow>("article_theme_row");
        let article_theme_event = builder.get::<EventBox>("article_theme_event");
        article_theme_event.set_events(EventMask::BUTTON_PRESS_MASK);

        // -----------------------------------------------------------------------
        // article view allow selection
        // -----------------------------------------------------------------------
        let allow_selection_switch = builder.get::<Switch>("allow_selection_switch");
        allow_selection_switch.set_state(settings.read().get_article_view_allow_select());

        // -----------------------------------------------------------------------
        // article view fonts
        // -----------------------------------------------------------------------
        let have_custom_font = settings.read().get_article_view_font().is_some();

        let font_row = builder.get::<ActionRow>("font_row");
        font_row.set_sensitive(have_custom_font);

        let font_button = builder.get::<FontButton>("font_button");
        font_button.set_sensitive(have_custom_font);
        if let Some(font) = settings.read().get_article_view_font() {
            font_button.set_font(&font);
        }

        let use_system_font_switch = builder.get::<Switch>("use_system_font_switch");
        use_system_font_switch.set_state(!have_custom_font);

        let views_page = ViewsPage {
            widget: views_page,
            delete_signal: Arc::new(RwLock::new(None)),
            sender: sender.clone(),
            settings: settings.clone(),

            feed_only_show_relevant_switch,
            feed_only_show_relevant_signal: Arc::new(RwLock::new(None)),

            article_order_pop,
            article_order_label,
            article_order_list,
            article_order_list_signal: Arc::new(RwLock::new(None)),
            article_order_event,
            article_order_event_signal: Arc::new(RwLock::new(None)),
            article_order_row,
            article_order_listbox_signal: Arc::new(RwLock::new(None)),

            show_thumbs_switch,
            show_thumbs_signal: Arc::new(RwLock::new(None)),

            article_theme_label,
            article_theme_row,
            article_theme_listbox_signal: Arc::new(RwLock::new(None)),
            article_theme_event,
            article_theme_event_signal: Arc::new(RwLock::new(None)),

            allow_selection_switch,
            allow_selection_switch_signal: Arc::new(RwLock::new(None)),

            font_row,
            font_button,
            font_button_signal: Arc::new(RwLock::new(None)),

            use_system_font_switch,
            use_system_font_switch_signal: Arc::new(RwLock::new(None)),
        };
        views_page.setup_close(parent);
        views_page.setup_feed_list();
        views_page.setup_article_list();
        views_page.setup_article_view();
        views_page
    }

    fn setup_close(&self, parent: &PreferencesWindow) {
        self.delete_signal.write().replace(
            parent.connect_delete_event(clone!(
                    @strong self.delete_signal as delete_signal,

                    @weak self.feed_only_show_relevant_switch as feed_only_show_relevant_switch,
                    @strong self.feed_only_show_relevant_signal as feed_only_show_relevant_signal,
                    @weak self.article_order_list as article_order_list,
                    @strong self.article_order_list_signal as article_order_list_signal,
                    @weak self.article_order_event as article_order_event,
                    @strong self.article_order_event_signal as article_order_event_signal,
                    @weak self.article_order_row as article_order_row,
                    @strong self.article_order_listbox_signal as article_order_listbox_signal,
                    @weak self.show_thumbs_switch as show_thumbs_switch,
                    @strong self.show_thumbs_signal as show_thumbs_signal,
                    @weak self.article_theme_event as article_theme_event,
                    @strong self.article_theme_event_signal as article_theme_event_signal,
                    @weak self.article_theme_row as article_theme_row,
                    @strong self.article_theme_listbox_signal as article_theme_listbox_signal,
                    @weak self.allow_selection_switch as allow_selection_switch,
                    @strong self.allow_selection_switch_signal as allow_selection_switch_signal,
                    @weak self.use_system_font_switch as use_system_font_switch,
                    @strong self.use_system_font_switch_signal as use_system_font_switch_signal,
                    @weak self.font_button as font_button,
                    @strong self.font_button_signal as font_button_signal => @default-panic, move |dialog, _event| {
                        GtkUtil::disconnect_signal(delete_signal.write().take(), dialog);

                        GtkUtil::disconnect_signal(feed_only_show_relevant_signal.write().take(), &feed_only_show_relevant_switch);
                        GtkUtil::disconnect_signal(article_order_list_signal.write().take(), &article_order_list);
                        GtkUtil::disconnect_signal(article_order_event_signal.write().take(), &article_order_event);
                        GtkUtil::disconnect_signal(show_thumbs_signal.write().take(), &show_thumbs_switch);
                        GtkUtil::disconnect_signal(article_theme_event_signal.write().take(), &article_theme_event);
                        GtkUtil::disconnect_signal(allow_selection_switch_signal.write().take(), &allow_selection_switch);
                        GtkUtil::disconnect_signal(use_system_font_switch_signal.write().take(), &use_system_font_switch);
                        GtkUtil::disconnect_signal(font_button_signal.write().take(), &font_button);

                        if let Some(listbox) = article_order_row.parent() {
                            if let Ok(listbox) = listbox.downcast::<ListBox>() {
                                GtkUtil::disconnect_signal(article_order_listbox_signal.write().take(), &listbox);
                            }
                        }
                        if let Some(listbox) = article_theme_row.parent() {
                            if let Ok(listbox) = listbox.downcast::<ListBox>() {
                                GtkUtil::disconnect_signal(article_theme_listbox_signal.write().take(), &listbox);
                            }
                        }

                        Inhibit(false)
                    }
                )
            )
        );
    }

    fn setup_feed_list(&self) {
        self.feed_only_show_relevant_signal
            .write()
            .replace(self.feed_only_show_relevant_switch.connect_state_set(clone!(
                @weak self.settings as settings,
                @strong self.sender as sender => @default-panic, move |_switch, is_set|
            {
                if settings.write().set_feed_list_only_show_relevant(is_set).is_err() {
                    Util::send(
                        &sender,
                        Action::ErrorSimpleMessage("Failed to set setting 'show relevant feeds'.".to_owned()),
                    );
                }
                Util::send(&sender, Action::UpdateSidebar);
                Inhibit(false)
            })));
    }

    fn setup_article_list(&self) {
        self.article_order_list_signal
            .write()
            .replace(self.article_order_list.connect_row_activated(clone!(
                @weak self.article_order_pop as article_order_pop,
                @weak self.article_order_label as article_order_label,
                @weak self.settings as settings,
                @strong self.sender as sender => @default-panic, move |_list, row| {
                article_order_pop.popdown();
                let new_order = match row.index() {
                    0 => ArticleOrder::NewestFirst,
                    _ => ArticleOrder::OldestFirst,
                };
                article_order_label.set_label(new_order.to_str());
                if settings.write().set_article_list_order(new_order).is_ok() {
                    Util::send(&sender, Action::UpdateArticleList);
                } else {
                    Util::send(
                        &sender,
                        Action::ErrorSimpleMessage("Failed to set setting 'article order'.".to_owned()),
                    );
                }
            })));

        self.article_order_event_signal
            .write()
            .replace(self.article_order_event.connect_button_press_event(clone!(
                @weak self.article_order_pop as article_order_pop => @default-panic, move |_eventbox, event|
            {
                if event.button() != 1 {
                    return Inhibit(false);
                }
                match event.event_type() {
                    EventType::ButtonRelease | EventType::DoubleButtonPress | EventType::TripleButtonPress => {
                        return Inhibit(false);
                    }
                    _ => {}
                }

                article_order_pop.popup();
                Inhibit(false)
            })));

        if let Some(listbox) = self.article_order_row.parent() {
            if let Ok(listbox) = listbox.downcast::<ListBox>() {
                self.article_order_listbox_signal
                    .write()
                    .replace(listbox.connect_row_activated(
                        clone!(@weak self.article_order_pop as article_order_pop => @default-panic, move |_list, row| {
                            if row.widget_name() == "article_order_row" {
                                article_order_pop.popup();
                            }
                        }),
                    ));
            }
        }

        self.show_thumbs_signal
            .write()
            .replace(self.show_thumbs_switch.connect_state_set(clone!(
                @weak self.settings as settings,
                @strong self.sender as sender => @default-panic, move |_switch, is_set|
            {
                if settings.write().set_article_list_show_thumbs(is_set).is_err() {
                    Util::send(
                        &sender,
                        Action::ErrorSimpleMessage("Failed to set setting 'show thumbnails'.".to_owned()),
                    );
                }
                Util::send(&sender, Action::UpdateArticleList);
                Inhibit(false)
            })));
    }

    fn setup_article_view(&self) {
        self.article_theme_event_signal
            .write()
            .replace(self.article_theme_event.connect_button_press_event(clone!(
                @weak self.settings as settings,
                @weak self.article_theme_label as article_theme_label,
                @strong self.sender as sender => @default-panic, move |eventbox, event|
            {
                if event.button() != 1 {
                    return Inhibit(false);
                }
                match event.event_type() {
                    EventType::ButtonRelease | EventType::DoubleButtonPress | EventType::TripleButtonPress => {
                        return Inhibit(false);
                    }
                    _ => {}
                }

                let theme_chooser = ThemeChooser::new(eventbox, &sender, &settings);
                let theme_chooser_close_signal = Arc::new(RwLock::new(None));
                theme_chooser_close_signal.write().replace(theme_chooser.widget().connect_closed(clone!(
                    @weak settings,
                    @weak article_theme_label,
                    @strong theme_chooser_close_signal,
                    @strong sender => @default-panic, move |pop|
                {
                    GtkUtil::disconnect_signal(theme_chooser_close_signal.write().take(), pop);
                    article_theme_label.set_label(settings.read().get_article_view_theme().name());
                    Util::send(&sender, Action::RedrawArticle);
                })));
                theme_chooser.widget().popup();

                Inhibit(false)
            })));

        if let Some(listbox) = self.article_theme_row.parent() {
            if let Ok(listbox) = listbox.downcast::<ListBox>() {
                self.article_theme_listbox_signal
                    .write()
                    .replace(listbox.connect_row_activated(clone!(
                        @weak self.article_theme_label as article_theme_label,
                        @weak self.article_theme_event as article_theme_event,
                        @weak self.settings as settings,
                        @strong self.sender as sender => @default-panic, move |_list, row|
                    {
                        if row.widget_name() == "article_theme_row" {
                            let theme_chooser = ThemeChooser::new(&article_theme_event, &sender, &settings);
                            let theme_chooser_close_signal = Arc::new(RwLock::new(None));
                            theme_chooser_close_signal.write().replace(theme_chooser.widget().connect_closed(clone!(
                                @strong sender,
                                @strong theme_chooser_close_signal,
                                @weak settings => @default-panic, move |pop|
                            {
                                GtkUtil::disconnect_signal(theme_chooser_close_signal.write().take(), pop);
                                article_theme_label.set_label(settings.read().get_article_view_theme().name());
                                Util::send(&sender, Action::RedrawArticle);
                            })));
                            theme_chooser.widget().popup();
                        }
                    })));
            }
        }

        self.allow_selection_switch_signal
            .write()
            .replace(self.allow_selection_switch.connect_state_set(clone!(
                @weak self.settings as settings,
                @strong self.sender as sender => @default-panic, move |_switch, is_set|
            {
                if settings.write().set_article_view_allow_select(is_set).is_ok() {
                    Util::send(&sender, Action::RedrawArticle);
                } else {
                    Util::send(
                        &sender,
                        Action::ErrorSimpleMessage("Failed to set setting 'allow article selection'.".to_owned()),
                    );
                }
                Inhibit(false)
            })));

        self.font_button_signal
            .write()
            .replace(self.font_button.connect_font_set(clone!(
                @weak self.settings as settings,
                @strong self.sender as sender => @default-panic, move |button| {
                let font = match button.font() {
                    Some(font) => Some(font.to_string()),
                    None => None,
                };
                if settings.write().set_article_view_font(font).is_ok() {
                    Util::send(&sender, Action::RedrawArticle);
                } else {
                    Util::send(
                        &sender,
                        Action::ErrorSimpleMessage("Failed to set setting 'article font'.".to_owned()),
                    );
                }
            })));

        self.use_system_font_switch_signal
            .write()
            .replace(self.use_system_font_switch.connect_state_set(clone!(
                @weak self.font_button as font_button,
                @weak self.font_row as font_row,
                @weak self.settings as settings,
                @strong self.sender as sender => @default-panic, move |_switch, is_set|
            {
                let font = if is_set {
                    None
                } else if let Some(font_name) = font_button.font() {
                    Some(font_name.to_string())
                } else {
                    None
                };
                font_button.set_sensitive(!is_set);
                font_row.set_sensitive(!is_set);
                if settings.write().set_article_view_font(font).is_ok() {
                    Util::send(&sender, Action::RedrawArticle);
                } else {
                    Util::send(
                        &sender,
                        Action::ErrorSimpleMessage("Failed to set setting 'use system font'.".to_owned()),
                    );
                }
                Inhibit(false)
            })));
    }
}
