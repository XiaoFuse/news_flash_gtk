use crate::app::Action;
use crate::error_dialog::ErrorDialog;
use crate::util::{BuilderHelper, GtkUtil};
use glib::{clone, Sender, SignalHandlerId};
use gtk::prelude::*;
use gtk::{Button, InfoBar, Label, ResponseType};
use log::error;
use news_flash::NewsFlashError;
use parking_lot::RwLock;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct ErrorBar {
    widget: InfoBar,
    label: Label,
    detail_button: Button,
    detail_signal: Arc<RwLock<Option<SignalHandlerId>>>,
    sender: Sender<Action>,
}

impl ErrorBar {
    pub fn new(builder: &BuilderHelper, sender: Sender<Action>) -> Self {
        let error_bar = ErrorBar {
            widget: builder.get::<InfoBar>("error_bar"),
            label: builder.get::<Label>("error_label"),
            detail_button: builder.get::<Button>("info_button"),
            detail_signal: Arc::new(RwLock::new(None)),
            sender,
        };

        error_bar.init();

        error_bar
    }

    fn init(&self) {
        self.widget.set_visible(true);
        self.widget.set_revealed(false);

        self.widget.connect_response(clone!(
            @weak self.detail_signal as detail_signal,
            @weak self.detail_button as detail_button => @default-panic, move |info_bar, response_type|
        {
            if response_type == ResponseType::Close {
                Self::close(
                    &info_bar,
                    &detail_button,
                    &detail_signal,
                );
            }
        }));
    }

    fn close(info_bar: &InfoBar, detail_button: &Button, detail_signal: &Arc<RwLock<Option<SignalHandlerId>>>) {
        info_bar.set_revealed(false);
        GtkUtil::disconnect_signal(detail_signal.write().take(), detail_button);
    }

    pub fn hide(&self) {
        self.widget.set_revealed(false);
    }

    pub fn simple_message(&self, message: &str) {
        self.label.set_text(message);
        self.widget.set_revealed(true);
        self.detail_button.set_visible(false);
    }

    pub fn news_flash_error(&self, message: &str, error: NewsFlashError) {
        self.label.set_text(message);
        self.widget.set_revealed(true);
        self.detail_button.set_visible(true);

        GtkUtil::disconnect_signal(self.detail_signal.write().take(), &self.detail_button);
        self.detail_signal
            .write()
            .replace(self.detail_button.connect_clicked(move |button| {
                if let Ok(parent) = GtkUtil::get_main_window(button) {
                    let _dialog = ErrorDialog::new(&error, &parent);
                } else {
                    error!("Failed to spawn ErrorDialog. Parent window not found.");
                }
            }));
    }
}
